////
 * Copyright (C) 2019 Eclipse Foundation, Inc. and others.
 * 
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * SPDX-FileType: SOURCE
 *
 * SPDX-FileCopyrightText: 2019 Eclipse Foundation, Inc.
 * SPDX-FileCopyrightText: 2019 Contributors to the Eclipse Foundation
 *
 * SPDX-License-Identifier: EPL-2.0
////

[[vulnerability]]
= Managing and Reporting Vulnerabilities

The {securityPolicyUrl}[Eclipse Foundation Vulnerability Reporting Policy] contains information regarding obligations and specific practices regarding the reporting and disclosure of vulnerabilities.

[[vulnerability-team]]
== Eclipse Foundation Security Team

The Eclipse Foundation Security Team provides help and advice to Eclipse projects on security issues and is the first point of contact for handling security vulnerabilities. Members of the Security Team are selected from committers on Eclipse Projects, members of the xref:roles-ac[Eclipse Architecture Council], and Eclipse Foundation staff.

You can contact the Eclipse Foundation Security Team by sending email to {securityTeamEmail} or by open an issue in the {vulnerabilityReportUrl}[general vulnerability issue tracker].

== Project Setup for Vulnerability Reporting

The default project setup is to use general Eclipse Foundation reporting (see below). The strong recommendation is to list reporting methods clearly inside a `SECURITY.md` file in the main repository (also in other repositories if it makes sense) to help security researchers to communicate with the project in a secure way. Similar information should be available in the project's documentation.

If the project decides to activate reporting via confidential issues (GitLab) or private security advisories (GitHub), please make a request via the {helpdeskUrl}[Help Desk]. The Eclipse Foundation Security team can train the project in using those means. When new reporting methods are set up, update your `SECURITY.md` accordingly.

In order to be able to set up, monitor, and also help projects dealing with security settings, new project are set up with members of the Eclipse Foundation Security team. Also, in GitHub repositories, if self-management is enabled, the project will include `.eclipsefdn` repository. Please refer to the xref:resources-github-self-service[documentation] for more information.

[[vulnerability-reporting]]
== Reporting

Vulnerabilities can be reported via a project-specific security tracker, or via general Eclipse Foundation means: an email to {securityTeamEmail} or and issue in the {vulnerabilityReportUrl}[general vulnerability issue tracker].

The general mailto:{securityTeamEmail}[security team email address] can be used to report vulnerabilities in any Eclipse Foundation project. Members of the Eclipse Foundation Security Team will receive messages sent to this address. This address should be used only for reporting undisclosed vulnerabilities; regular issue reports and questions unrelated to vulnerabilities in Eclipse Foundation project software will be ignored. Note that this email address is not encrypted.

The community is encouraged to report vulnerabilities using the standard project-specific issue tracker. The appropriate link should be available in the project's `SECURITY.md` file. In case of a doubt, use the {vulnerabilityReportUrl}[general vulnerability issue tracker].

[NOTE]
====
Security vulnerabilities should be reported as confidential issues (on GitLab), private security advisories (GitHub) or other confidential means. If unsure, use the {vulnerabilityReportUrl}[general vulnerability issue tracker] with the default template.
====

[[vulnerability-disclosure]]
== Disclosure

Disclosure is initially limited to the reporter, the project team and the Eclipse Foundation Security Team, but is expanded to include other individuals, and the general public. The timing and manner of disclosure is governed by the {securityPolicyUrl}[Eclipse Foundation Vulnerability Reporting Policy].

Publicly disclosed issues are listed on the {knownVulnerabilitiesUrl}[Known Eclipse Security Vulnerabilities] page.

[[vulnerability-cve]]
== Common Vulnerabilities and Exposure (CVE)

The Eclipse Foundation is a {cveUrl}[Common Vulnerabilities and Exposures] (CVE) Numbering Authority.

A unique vulnerability number like a CVE allows developers, distributions and security researchers to talk about the same issue using a common name. It also helps the project documentation.

The project team can ask for a number when they learn about the vulnerability: by receiving a report or simply by discovering it. They can request the number before the fix is ready, as the request will be confidential until the project tells the Eclipse Foundation Security Team to publish it.

[TIP]
====
If you're not sure whether or not a CVE is required, err on the side of caution and request one.

A CVE number related to a bugfix release gives users a clear sign that an update is strongly recommended.

Think of a first CVE for your open source project as a rite of passage. Alerting your adopters of a vulnerability is a signal of maturity and responsibility to the open source community.
====

Project can request CVE numbers in different ways depending on the project setup.

=== General case

For most projects the process looks as follows:

* *Request the number*: When you know that there is a security issue (the fix is not yet needed at this point), go to {cveRequestUrl}[the CVE Request form] and fill it. Make sure to keep the issue confidential for now. If you want to make the issue visible to multiple project members, add them as assignees when creating the ticket (it will be hard later on).

* *Receive the number*: The Eclipse Foundation Security Team will come back to you with the assigned number. You can start using it internally when preparing documentation of the release containing the fix.

* *Prepare the fix* (with backports to all supported branches) and the release: The common practice is to avoid using the CVE number in the fix commit message or the fix itself. Use generic terms instead. The goal is to allow time for all users to upgrade without giving potential attackers too much information. See also the next step (you can do them in parallel).

* *Ask for publication of the issue*: When you have the fix ready and published, update the ticket. Make sure that you specify the version (or versions) where the bug is fixed, including all stable branches. Fill in the description of the issue. You may also consider removing some of the comments (for example containing sensitive or incorrect information). At this stage you may decide to publish only partial information (for example without the link to the commit with the fix). Then ask the security team to make the CVE and the discussion of the CVE assignment public. Ideally your release and the publication happen at the same time. You can ask for a publication at your specific date, for example at your planned release date.

* *See it publicly*: The ticket becomes public and the Eclipse Foundation Security Team publishes the CVE entry.

* (Optional, a few days later) *Update the entry* with all additional information that might be helpful. That includes, for example, the link to the fix commit.

[NOTE]
====
Project committers should make it clear if their request is a reservation of a CVE number, or a publication.
====

[TIP]
====
When working with GitLab confidential issues, be careful. Only the author of the ticket, all committers in the project containing the issue tracker, and assigned people will see the issue and receive notifications.

Adding new assignees who are not members of the project has no effect when the issue is already confidential. When in doubt, verify with the concerned person if they receive notifications by other communication means.
====

The _Tracking_ section of the request issue includes some checkboxes for the project team and for the Security Team. The Security Team will assign the CVE upon receipt of the request, but will not escalate (that is, they will not report the assignment of the CVE to the central authority) until after a project committer clearly indicates that they are ready to disclose the vulnerability.

****
...

*This section will completed by the project team*.

* [ ] Reserve an entry only
* [ ] We're ready for this issue to be reported to the central authority (i.e., make this public now)
* [ ] (when applicable) The GitHub Security Advisory is ready to be published now

...
****

Check the second checkbox when you're ready for the Eclipse Foundation Security Team to submit the request to the central authority (that is, when you're ready for the issue to be disclosed publicly).

If the project is using a {githubSecurityAdvisoryInfoUrl}[GitHub Security Advisory] to track mitigation of the vulnerability, the Security Team intervention will likely be required to submit the advisory. Click the second checkbox to indicate that you're ready for the Security Team to submit the advisory on the project team's behalf.

=== Projects Using Exclusively GitHub Security Advisories

If the project receives reports exclusively by private reporting with GitHub Security Advisories, the procedure is as follows:

* *Prepare the initial data*: The project receives initial data in the report and assesses that it is a real vulnerability.

* *Request the number*: When you know that there is a security issue (the fix is not yet needed at this point), contact the Eclipse Foundation Security Team to request the number.

* *Receive the number*: The Eclipse Foundation Security Team will request a number and GitHub will assign one (or ask for more information in the report). You can start using it internally when preparing documentation of the release containing the fix.

* *Prepare the fix* (with backports to all supported branches) and the release: The common practice is to avoid using the CVE number in the fix commit message or the fix itself. Use generic terms instead. The goal is to allow time for all users to upgrade without giving potential attackers too much information. See also the next step (you can do them in parallel).

* *Ask for publication of the issue*: When you have the fix ready and published, as the Eclipse Foundation Security team to publish the advisory. Make sure that you specify the version (or versions) where the bug is fixed, including all stable branches. Fill in the description of the issue. You may also consider removing some of the comments (for example containing sensitive or incorrect information). At this stage you may decide to publish only partial information (for example without the link to the commit with the fix). Ideally your release happens earlier or at the same time as the publication. You can ask for a publication at your specific date, for example at your planned release date.

* *See it publicly*: The advisory becomes public, so is the CVE entry.

* (Optional, a few days later) *Update the entry* with all additional information that might be helpful. That includes, for example, the link to the fix commit.

=== Required Information

For all types of request, the project needs to prepare information about the vulnerability.

The request for a publication must provide:

* The name of the impacted project and product;
* A description of the versions impacted (which may include ranges);
* A {cweUrl}[Common Weakness Enumeration] (CWE) code;
* A one or two sentence summary of the issue which clearly identifies the Eclipse project/product and impacted versions; and
* A pointer (URL) to the issue used to track mitigation of the vulnerability.

The request may optionally include additional information, such as a Common Vulnerability Scoring System (CVSS) code, a link to the commit fixing the issue etc.

.Example CVE Report data
----
Project name: Eclipse Vert.x

Project id: rt.vertx

Versions affected: [3.0, 3.5.1]

Common Weakness Enumeration:

- CWE-93: Improper Neutralization of CRLF Sequences ('CRLF Injection')

Summary:

In Eclipse Vert.x version 3.0 to 3.5.1, the HttpServer response
headers and HttpClient request headers do not filter carriage return and
line feed characters from the header value. This allow unfiltered values
to inject a new header in the client request or server response.

Links:

- https://gitlab.eclipse.org/eclipse/myproject/-/issues/12345
----

If unsure on how to fill the form, ask the Eclipse Foundation Security Team for assistance.

[[security-faq]]
== Frequently Asked Questions

[qanda]

In what form should a disclosure be published? Is publishing the bug on the {knownVulnerabilitiesUrl}[Known Vulnerabilities page] enough? ::

Publishing on the Known Vulnerabilities page will happen automatically. This is minimal disclosure. Whether or not you should do more is a project team decision. If you  need help with that decision, connect with your xref:roles-pmc[Project Management Committee] (PMC) or the Security Team.
+
If the vulnerability real and is in release software (i.e., it's likely to have been adopted), you should xref:vulnerability-cve[request a CVE].
+
You should let your community know about the vulnerability though your usual communication channels.
 
Can I already commit the fixes to our repository and provide a service release, or shall I wait for some part of the disclosure process first? ::

In general, you should fix the issue first and prepare a bugfix release.
+
Whether or not we disclose in advance of making the fix available is a judgement call. Many projects notify of a date of an imminent security bugfix release in advance, without releasing the details. Users can then plan their update. When there is a real risk that somebody may exploit the vulnerability, you generally want to inform your adopters as quickly and discretely as possible so that they can prepare themselves.
+
If the issue is particularly sensitive and you need to make that fix in a private repository and coordinate disclosure, connect with Security Team and we'll help. Very few projects actually need to go to this extreme.
 
Is there something specific I should add (or something I should avoid mentioning) in the commit message? ::

That depends. In general, you should avoid adding anything that calls particular attention to the vulnerability. Just state what the commit contains. Especially avoid expressions like `vulnerability` or using a reserved CVE number.
 
Do we need a xref:vulnerability-cve[CVE]? ::

It's up to the project team. We need the project team to engage with the process of gathering the information required to report the vulnerability to the central authority; the first step in that process is deciding whether or not a CVE is desired/required.
+
The general rule is that a CVE is required when a vulnerability impacts release software. The Eclipse Foundation Security Team has given this advice (paraphrased):
+
[quote]
____
If someone can download compiled (e.g., JAR) files and use them without any sort of compilation process then we are inclined to say that there exists a tangible risk to consumers and so a CVE should be requested. That is, unless that version string specifically says alpha or beta, or the content has otherwise clear warnings to not use it in a production context, then we should -- as good citizens -- create a CVE. Merely being versioned 0.x instead of 1.x doesn't absolve the situation.
____
+
If you're not sure, check with your PMC or the xref:vulnerability-team[Security Team].
+
It's a bit of a rite of passage for an open source project to disclose their first vulnerability. If in doubt, ask the Eclipse Foundation Security Team for guidance.

Do we need a xref:vulnerability-cve[CVE] for versions of software that we released before moving our project to the Eclipse Foundation? ::

The answer to this is not obvious, but as a general rule... no. The answer is not obvious because the continuity of the source of affected products may not be obvious (or relevant) to consumers, and it is not strictly wrong for a CVE Numbering Authority to create a CVE for a version of a product not immediately in their purview. 
+
Ultimately, whether or not we should create a CVE is the project team's call.

I think that my project hasn't received all notifications about security issues? ::

In this case, please contact the xref:vulnerability-team[Security Team]. Please note that the Eclipse Foundation Security Team notifies project leads by default.

I can't see a confidential issue in the Eclipse Foundation GitLab instance ::

If you have received a notification of a confidential issue by email, make sure to log in to the Eclipse Foundation instance of GitLab first, before following the link. Otherwise you will get a 404 error page.
+
If you are following a link, verify with the person managing the issue (for example the reporter) if you have been added to the issue. Currently, in the general issue tracker, each person needs to be added manually. In case of difficulties, ask the Eclipse Foundation Security team for assistance.
+
Also please note that we need an activated GitLab account to add a user to a confidential issue. In practice, the user needs to log in into the Eclipse Foundation GitLab instance at least once.

Confidential issues are not visible to all committers ::

Currently, in the general issue tracker, each person needs to be added manually. In case of difficulties, ask the Eclipse Foundation Security team for assistance.

I can see `.eclipsefdn` repository in my project. What is this? ::

This repository is used to keep project management data. See the related xref:resources-github-self-service[documentation] or ask the Eclipse Foundation staff for help.
